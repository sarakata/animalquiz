export class AnimalQuestion {
    animal: string;
    options: string[];
    correctOption: number;
}