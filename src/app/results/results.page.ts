import { Component, OnInit } from '@angular/core';
import {Route, ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit {


  
  questionCounter: number;
  correctAnsCounter: number;
  duration: number;

  constructor(public router: Router, public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

}
