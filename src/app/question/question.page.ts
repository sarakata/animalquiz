import { Component, OnInit } from '@angular/core';
import { AnimalQuestion } from '../../animalquestion';
import { ResultsPage } from '../results/results.page';
import { Router } from '@angular/router';


@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})

export class QuestionPage implements OnInit {

  questions: AnimalQuestion[] = [];
  activeQuestion: AnimalQuestion;
  isAnswered: false;
  isCorrect: boolean;
  feedback: string;
  questionCounter: number;
  correctAnsCounter: number;
  start: Date;
  end: Date;
  duration: number;

  constructor(public router: Router) {}

  results() {
    this.router.navigateByUrl('results')
  }

  ngOnInit() {
    
    fetch('../../assets/data/questions.json').then(res => res.json())
    .then(json => {
      this.questions = json;
      console.log(this.questions.length);
      this.setQuestion();
    });

    this.questionCounter = 0;
    this.correctAnsCounter = 0;
    this.start = new Date();

  }

 /*  }
  ngOnInit() {

    fetch('../../assets/data/questions.json').then(res => res.json())
    .then(json => {
      this.questions = json;
      console.log(this.questions.length);
      this.setQuestion();
    });
    this.questionCounter = 0;
    this.start = new Date();
  } 
 */

  setQuestion() {
    if (this.questionCounter === this.questions.length) {
        this.end = new Date();
        this.duration = this.end.getTime() - this.start.getTime();
        this.router.navigateByUrl('results/' + this.duration + '/' + this.questionCounter);
        this.ngOnInit();
    } else {
      this.isAnswered = false;
      this.feedback = '';
      this.isCorrect = false;
      this.activeQuestion = this.questions[this.questionCounter];
      this.questionCounter++;
    }
  }
  
  // setQuestion() {
  //   if (this.questionCounter === this.questions.length) {
  //       this.end = new Date();
  //       this.duration = this.end.getTime() - this.start.getTime();
  //       this.router.navigateByUrl('results/' + this.duration);
  //       this.ngOnInit();
  //   } else {
  //     this.feedback = '';
  //     this.isCorrect = false;
  //     this.activeQuestion = this.questions[this.questionCounter];
  //     this.questionCounter++;
  //   }
  // }
  

   checkOption(option: number, activeQuestion: AnimalQuestion) {
    if (option === activeQuestion.correctOption) {
      this.isCorrect = true;
      this.feedback = activeQuestion.options[option] +
      '  Correct! Tap the paw and go to the next question!';
      this.correctAnsCounter++;
      this.isAnswered = true;
      
    } else {
      this.isCorrect = false;
      this.feedback = '  Incorrect! Tap the paw and go to the next question!' +
        (this.questionCounter === this.questions.length);
    }
  }

  }
  